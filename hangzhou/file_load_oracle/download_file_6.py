#!/usr/bin/env python
# -*-coding:utf-8 -*-
# ***************************************************************************
# 文件名称：download_file_6.py
# 功能描述：将脚本拷贝到远程服务器，随后日志删除脚本
# 输 入 表：
# 输 出 表：
# 创 建 者：hyn
# 创建日期：20200318
# 修改日志：
# 修改日期：
# ***************************************************************************
# 程序调用格式：sh download_file_6.sh table_name
# ***************************************************************************

# 1.配置textfile临时表，分区字段转为普通字段
# 1.下载集群文件
# 2.合并文件
# 3.传文件到82机器

import sys
import time
import datetime

# 常量定义
hdfs_path = '/inceptor1/user/hive/warehouse/dw.db/dacp'
local_path = '/home/hive/hyn/oracle/demo'
file_name = 'test.dat'


# 下载集群文件
def download_file(table_name):
    download_file = 'hadoop fs -get %s/%s/* %s' % (hdfs_path, table_name, local_path)
    print download_file


# 合并文件
def combine_file():
    combine_sh = 'cat %s/* >> %s/' % (local_path, local_path)
    print combine_sh


# 传文件到82机器，调用ftp脚本传输，用平台组件进行文件传输
# def cp_file_82():


if __name__ == '__main__':

    input_length = len(sys.argv)
    print 'input_str: ', len(sys.argv)

    if input_length == 2:
        table_name = sys.argv[1]
        print table_name
        download_file(table_name)

    else:
        print '输入参数有误'
