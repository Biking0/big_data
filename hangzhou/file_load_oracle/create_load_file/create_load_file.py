#!/usr/bin/env python
# -*-coding:utf-8 -*-
# ***************************************************************************
# 文件名称：create_load_file.py
# 功能描述：创建ctl文件，sqlldr shell执行脚本
# 输 入 表：
# 输 出 表：
# 创 建 者：hyn
# 创建日期：20211016
# 修改日志：
# 修改日期：
# ***************************************************************************
# 程序调用格式：python create_load_file.py table_name
# ***************************************************************************

table_name = "ac83_temp1"
path = '/home/dacp/'

# 并发参数
num = 5

# 常量定义
line_1 = "OPTIONS (rows=10240,direct=TRUE,parallel=TRUE,multithreading=TRUE,bindsize=153600000,readsize=307200000,PARALLEL=TRUE)"
line_2 = "load data"
line_3 = "CHARACTERSET AL32UTF8"
line_4 = "infile '%s" % (path)
line_5 = "append into table %s" % (table_name)
line_6 = "Fields terminated by \"\\t\""
line_7 = "Optionally enclosed by '\"'"
line_8 = "trailing nullcols"
line_9 = "(aaz219,aaz661,aaz257,aaz099,aae140,aac001,aae002,aae041,aae042,aac309,aaa079,aae696,aaa036,aaa345,aaa346,aae019,aae013,aaz649,aae860,aae859,aae011,aaz692,aae036,aab034,aab360,aab359,aaf018,aaa431,aaz673,aaa027,aaa508,aaa350)"

# sqlldr_sh = "sqlldr einp_treatment/einp_treatment@10.85.83.93:1521/GTCSPDB control =101102.ctl log=101102.log"
sqlldr_sh = "sqlldr einp_treatment/einp_treatment@10.85.83.93:1521/GTCSPDB control ="


def create_ctl():
    for i in range(num):
        f = open(str(i) + '.ctl', 'w')
        f.write(line_1 + '\n')
        f.write(line_2 + '\n')
        f.write(line_3 + '\n')
        f.write(line_4 + str(i) + '.dat\'' + '\n')
        f.write(line_5 + '\n')
        f.write(line_6 + '\n')
        f.write(line_7 + '\n')
        f.write(line_8 + '\n')
        f.write(line_9 + '\n')
        f.close()


def create_sh():
    # sqlldr_sh = "sqlldr einp_treatment/einp_treatment@10.85.83.93:1521/GTCSPDB control =101102.ctl log=101102.log"
    sqlldr_sh = "sqlldr einp_treatment/einp_treatment@10.85.83.93:1521/GTCSPDB control ="

    sqlldr_start = open('sqlldr_start.txt', 'w')

    for i in range(num):
        sh_name = str(i) + '.sh'

        f = open(sh_name, 'w')
        f.write(sqlldr_sh + '%s.ctl log=%s.log' % (str(i), str(i)) + '\n')
        f.close()
        sqlldr_start.write('nohup sh %s &' % (sh_name) + '\n')

    sqlldr_start.close()


create_ctl()
create_sh()
